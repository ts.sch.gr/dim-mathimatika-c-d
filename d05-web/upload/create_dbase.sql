# phpMyAdmin SQL Dump
# version 2.5.6
# http://www.phpmyadmin.net
#
# �������: localhost
# ������ �����������: 20 ��� 2004, ���� 08:32 PM
# ������ ����������: 4.0.18
# ������ PHP: 4.3.5
# 
# ���� : `mathgd`
# 

# --------------------------------------------------------

#
# ���� ������ ��� ��� ������ `activities`
#

CREATE TABLE `activities` (
  `id` int(11) NOT NULL auto_increment,
  `cat_id` int(11) default '1',
  `activity_name` varchar(255) default '-',
  `school_name` varchar(100) default '-',
  `teacher_name` varchar(100) default '-',
  `teacher_email` varchar(100) default '-',
  `password` varchar(10) default NULL,
  `approved` int(11) default '0',
  `date_uploaded` date default '0000-00-00',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

#
# '�������� ��������� ��� ������ `activities`
#


# --------------------------------------------------------

#
# ���� ������ ��� ��� ������ `categories`
#

CREATE TABLE `categories` (
  `id` int(11) NOT NULL auto_increment,
  `class_name` char(1) default '�',
  `section_name` varchar(255) default '-',
  `directory_name` varchar(255) default '-',
  `section_icon` varchar(255) default '-',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=24 ;

#
# '�������� ��������� ��� ������ `categories`
#

INSERT INTO `categories` (`id`, `class_name`, `section_name`, `directory_name`, `section_icon`) VALUES (1, '�', '����� ��� ������� �������', 'grafi_fysikwn_arithmwn', 'top_middle_7.png'),
(2, '�', '��������������� ������� �������', 'pollaplasiasmos_fysikwn_arithmwn', 'top_middle_5.png'),
(3, '�', '��������� �������', 'dekadikoi', 'top_middle_11.png'),
(4, '�', '������', 'motiba', 'top_middle_4.png'),
(5, '�', '�������� ��� ������� �������', 'sygkrisi_fysikwn', 'top_middle_1.png'),
(6, '�', '�������� ������� �������', 'diairesh', 'top_middle_6.png'),
(7, '�', '���������', 'metriseis', 'top_middle_10.png'),
(8, '�', '���������', 'gewmetria', 'top_middle_3.png'),
(9, '�', '�������� ��� �������� �� �������� ��������', 'prosthesi_fysikwn_arithmwn', 'top_middle_12.png'),
(10, '�', '��������� ��� ��������� �������', 'nomismata_dekadikoi', 'top_middle_9.png'),
(11, '�', '��������', 'klasmata', 'top_middle_8.png'),
(12, '�', '����� ��� ������� �������', 'grafi_fysikwn_arithmwn', 'top_middle_7.png'),
(13, '�', '��������������� ������� �������', 'pollaplasiasmos_fysikwn_arithmwn', 'top_middle_5.png'),
(14, '�', '��������� �������', 'dekadikoi', 'top_middle_11.png'),
(15, '�', '������', 'motiba', 'top_middle_4.png'),
(16, '�', '�������� �� ������� �������', 'sygkrisi_fysikwn', 'top_middle_1.png'),
(17, '�', '�������� ������� �������', 'diairesh', 'top_middle_6.png'),
(18, '�', '���������', 'metriseis', 'top_middle_10.png'),
(19, '�', '���������', 'gewmetria', 'top_middle_3.png'),
(20, '�', '�������� ��� �������� �� �������� ��������', 'prosthesi_fysikwn_arithmwn', 'top_middle_12.png'),
(21, '�', '��������� ��� ��������� �������', 'nomismata_dekadikoi', 'top_middle_9.png'),
(22, '�', '��������', 'klasmata', 'top_middle_8.png'),
(23, '�', '������� ��� ����������� ���������', 'syllogi_dedomenwn', 'top_middle_2.png');
